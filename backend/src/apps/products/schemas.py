from typing import Optional, List
from pydantic import BaseModel, validator
from fastapi_storages.integrations.sqlalchemy import FileType
from apps.colors.schemas import BaseColorSchema
from apps.subcategery.schemas import SubcategorySchema

class ProductSchemaBasket(BaseModel):
    id: Optional[int] = None
    name: Optional[str] = None
    price: Optional[int] = None

class ProductSchema(BaseModel):
    id: Optional[int] = None
    name: Optional[str] = None
    description: Optional[str] = None
    price: Optional[int] = None
    length: Optional[int] = None
    width: Optional[int] = None
    height: Optional[int] = None
    manufacturer: Optional[str] = None
    is_best_seller: Optional[bool] = None
    is_clearance: Optional[bool] = None

    color_1: Optional['BaseColorSchema'] = None
    images_1: Optional[List[str]] = None
    article_1: Optional[str] = None
    color_2: Optional['BaseColorSchema'] = None
    images_2: Optional[List[str]] = None
    article_2: Optional[str] = None
    color_3: Optional['BaseColorSchema'] = None
    images_3: Optional[List[str]] = None
    article_3: Optional[str] = None
    color_4: Optional['BaseColorSchema'] = None
    images_4: Optional[List[str]] = None
    article_4: Optional[str] = None
    color_5: Optional['BaseColorSchema'] = None
    images_5: Optional[List[str]] = None
    article_5: Optional[str] = None
    color_6: Optional['BaseColorSchema'] = None
    images_6: Optional[List[str]] = None
    article_6: Optional[str] = None
    color_7: Optional['BaseColorSchema'] = None
    images_7: Optional[List[str]] = None
    article_7: Optional[str] = None
    color_8: Optional['BaseColorSchema'] = None
    images_8: Optional[List[str]] = None
    article_8: Optional[str] = None
    color_9: Optional['BaseColorSchema'] = None
    images_9: Optional[List[str]] = None
    article_9: Optional[str] = None

    @validator('images_1', 'images_2', 'images_3', 'images_4', 'images_5', 'images_6', 'images_7', 'images_8',
               'images_9', pre=True)
    def filter_null_and_empty_lists(cls, v):
        if v is None or v == [None]:
            return []
        return v

    subcategory: Optional['SubcategorySchema'] = None

    class Config:
        from_attributes = True
        arbitrary_types_allowed = True

class ProductPagination(BaseModel):
    items: List[ProductSchema]
    total: int
    page: int
    page_size: int

    class Config:
        from_attributes = True
        arbitrary_types_allowed = True