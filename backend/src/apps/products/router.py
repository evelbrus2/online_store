import logging
from typing import Optional

from fastapi import APIRouter, Query, Depends, status, HTTPException

from config.database import get_db
from config.config import settings
from sqlalchemy.ext.asyncio import AsyncSession

from .dao import get_product_by_id, get_all_products, get_best_seller_products, get_clearance_products
from .schemas import ProductSchema, ProductPagination



# from .dao import
# from .schemas import

log_level = logging.DEBUG if settings.ENV == 'development' else logging.INFO

logging.basicConfig(level=log_level)
logger = logging.getLogger(__name__)

router = APIRouter(
    prefix="/Products",
    tags=["Продукты"]
)

@router.get("", response_model=ProductPagination)
async def read_all_products(page: int = 1, page_size: int = 10, subcategory_id: Optional[int] = None,
                            name: Optional[str] = None, session: AsyncSession = Depends(get_db)):
    return await get_all_products(session, page, page_size, subcategory_id, name)

@router.get("/best-sellers", response_model=ProductPagination)
async def read_best_seller_products(page: int = 1, page_size: int = 10, subcategory_id: Optional[int] = None,
                            name: Optional[str] = None, session: AsyncSession = Depends(get_db)):
    return await get_best_seller_products(session, page, page_size, subcategory_id, name)

@router.get("/clearance", response_model=ProductPagination)
async def read_clearance_products(page: int = 1, page_size: int = 10, subcategory_id: Optional[int] = None,
                            name: Optional[str] = None,
                                  session: AsyncSession = Depends(get_db)):
    return await get_clearance_products(session, page, page_size, subcategory_id, name)

@router.get("/{product_id}", response_model=ProductSchema)
async def read_product(product_id: int, session: AsyncSession = Depends(get_db)):
    product = await get_product_by_id(product_id, session)
    if product is None:
        # Правильный порядок позиционных аргументов для HTTPException
        raise HTTPException(status.HTTP_404_NOT_FOUND, f"Продукта c id {product_id} не существует")
    return product