import asyncio
import uuid
from sqlalchemy.future import select

from sqlalchemy import Column, Integer, String, ForeignKey, Boolean, LargeBinary, ARRAY
from sqlalchemy.orm import relationship
import random
import string

from fastapi_storages.integrations.sqlalchemy import FileType
from fastapi_storages import FileSystemStorage


from config.database import async_session_maker

storage = FileSystemStorage(path="/home/valentin/Desktop/FastApi/FAOS3/Canada/canada/backend/src/apps/images/uploads")

from sqlalchemy_searchable import search
from sqlalchemy_utils.types import TSVectorType

# from src.apps.subcategery.models import Subcategory
from config.database import Base

def generate_article():
    letters = string.ascii_letters.upper()  # Используем только заглавные буквы
    digits = string.digits

    # Выбираем четыре случайные буквы
    random_letters = ''.join(random.choice(letters) for i in range(4))

    # Выбираем пять случайных цифр
    random_digits = ''.join(random.choice(digits) for i in range(5))

    return random_letters + random_digits

class Product(Base):
    __tablename__ = "products"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, index=True)
    description = Column(String, index=True)
    price = Column(Integer)
    length = Column(Integer)
    width = Column(Integer)
    height = Column(Integer)
    manufacturer = Column(String)
    is_best_seller = Column(Boolean, default=False)
    is_clearance = Column(Boolean, default=False)

    # Отношения к цветам и изображениям

    color_1_id = Column(Integer, ForeignKey("base_color.id"), default=None)
    images_1 = Column(ARRAY(FileType(storage=storage)), nullable=True)
    article_1 = Column(String, unique=True, index=True)
    color_2_id = Column(Integer, ForeignKey("base_color.id"), default=None)
    images_2 = Column(ARRAY(FileType(storage=storage)), nullable=True)
    article_2 = Column(String, unique=True, index=True)
    color_3_id = Column(Integer, ForeignKey("base_color.id"), default=None)
    images_3 = Column(ARRAY(FileType(storage=storage)), nullable=True)
    article_3 = Column(String, unique=True, index=True)
    color_4_id = Column(Integer, ForeignKey("base_color.id"), default=None)
    images_4 = Column(ARRAY(FileType(storage=storage)), nullable=True)
    article_4 = Column(String, unique=True, index=True)
    color_5_id = Column(Integer, ForeignKey("base_color.id"), default=None)
    images_5 = Column(ARRAY(FileType(storage=storage)), nullable=True)
    article_5 = Column(String, unique=True, index=True)
    color_6_id = Column(Integer, ForeignKey("base_color.id"), default=None)
    images_6 = Column(ARRAY(FileType(storage=storage)), nullable=True)
    article_6 = Column(String, unique=True, index=True)
    color_7_id = Column(Integer, ForeignKey("base_color.id"), default=None)
    images_7 = Column(ARRAY(FileType(storage=storage)), nullable=True)
    article_7 = Column(String, unique=True, index=True)
    color_8_id = Column(Integer, ForeignKey("base_color.id"), default=None)
    images_8 = Column(ARRAY(FileType(storage=storage)), nullable=True)
    article_8 = Column(String, unique=True, index=True)
    color_9_id = Column(Integer, ForeignKey("base_color.id"), default=None)
    images_9 = Column(ARRAY(FileType(storage=storage)), nullable=True)
    article_9 = Column(String, unique=True, index=True)

    # Отношение к модели BaseColor
    color_1 = relationship("BaseColor", foreign_keys=[color_1_id], backref="products_1")
    color_2 = relationship("BaseColor", foreign_keys=[color_2_id], backref="products_2")
    color_3 = relationship("BaseColor", foreign_keys=[color_3_id], backref="products_3")
    color_4 = relationship("BaseColor", foreign_keys=[color_4_id], backref="products_4")
    color_5 = relationship("BaseColor", foreign_keys=[color_5_id], backref="products_5")
    color_6 = relationship("BaseColor", foreign_keys=[color_6_id], backref="products_6")
    color_7 = relationship("BaseColor", foreign_keys=[color_7_id], backref="products_7")
    color_8 = relationship("BaseColor", foreign_keys=[color_8_id], backref="products_8")
    color_9 = relationship("BaseColor", foreign_keys=[color_9_id], backref="products_9")

    # Отношение к подкатегориям и корзинам (при необходимости)
    subcategory_id = Column(Integer, ForeignKey("subcategories.id"))
    subcategory = relationship("Subcategory", back_populates="product")
    cart_products = relationship("CartProduct", back_populates="product")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.article_1 = self.generate_article()  # Генерировать артикул 1
        self.article_2 = self.generate_article()  # Генерировать артикул 2
        self.article_3 = self.generate_article()  # Генерировать артикул 3
        self.article_4 = self.generate_article()  # Генерировать артикул 4
        self.article_5 = self.generate_article()  # Генерировать артикул 5
        self.article_6 = self.generate_article()  # Генерировать артикул 6
        self.article_7 = self.generate_article()  # Генерировать артикул 7
        self.article_8 = self.generate_article()  # Генерировать артикул 8
        self.article_9 = self.generate_article()  # Генерировать артикул 9

    @staticmethod
    def generate_article():
        # Генерация уникального артикула
        return generate_article()

    def __str__(self):
        return f"{self.name}"

    class Config:
        orm_mode = True




