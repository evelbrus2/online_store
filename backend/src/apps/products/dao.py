from typing import Optional

from sqlalchemy import func
from sqlalchemy.future import select
from sqlalchemy.exc import NoResultFound
from .models import Product  # Импортируйте модель Product
from config.database import get_db
from sqlalchemy.orm import selectinload

from .schemas import ProductPagination, ProductSchema


async def get_product_by_id(product_id: int, session):
    query = select(Product).where(Product.id == product_id).options(
        selectinload(Product.color_1),
        selectinload(Product.color_4),
        selectinload(Product.subcategory)
    )
    result = await session.execute(query)
    try:
        product = result.scalar_one()
        return product
    except NoResultFound:
        return None

async def get_all_products(session, page: int = 1, page_size: int = 10, subcategory_id: Optional[int] = None, name: Optional[str] = None):
    offset = (page - 1) * page_size

    query = select(Product)

    if subcategory_id is not None:
        query = query.where(Product.subcategory_id == subcategory_id)

    if name and len(name) >= 2:  # Убедитесь, что строка поиска содержит хотя бы 2 символа
        query = query.where(Product.name.ilike(f"{name}%"))  # Изменено для поиска с начала строки

    total_query = select(func.count()).select_from(query.subquery())
    items_query = query.offset(offset).limit(page_size).options(
        selectinload(Product.color_1),
        selectinload(Product.color_4),
        selectinload(Product.subcategory)
    )

    total_result = await session.execute(total_query)
    total = total_result.scalar_one()

    items_result = await session.execute(items_query)
    items = items_result.scalars().all()

    return ProductPagination(
        items=[ProductSchema.from_orm(item) for item in items],
        total=total,
        page=page,
        page_size=page_size
    )



async def get_best_seller_products(session, page: int = 1, page_size: int = 10, subcategory_id: Optional[int] = None, name: Optional[str] = None):
    offset = (page - 1) * page_size

    query = select(Product).where(Product.is_best_seller == True)  # Фильтр для лучших продавцов

    if subcategory_id is not None:
        query = query.where(Product.subcategory_id == subcategory_id)

    if name and len(name) >= 2:  # Убедитесь, что строка поиска содержит хотя бы 2 символа
        query = query.where(Product.name.ilike(f"{name}%"))

    # Используйте модифицированный запрос для подсчета и выборки
    total_query = select(func.count()).select_from(query.subquery())
    items_query = query.offset(offset).limit(page_size).options(
        selectinload(Product.color_1),
        selectinload(Product.color_4),
        selectinload(Product.subcategory)
    )

    total_result = await session.execute(total_query)
    total = total_result.scalar_one()

    items_result = await session.execute(items_query)
    items = items_result.scalars().all()

    return ProductPagination(
        items=[ProductSchema.from_orm(item) for item in items],
        total=total,
        page=page,
        page_size=page_size
    )


async def get_clearance_products(session, page: int = 1, page_size: int = 10, subcategory_id: Optional[int] = None, name: Optional[str] = None):
    offset = (page - 1) * page_size

    # Добавляем фильтр is_clearance к начальному запросу
    query = select(Product).where(Product.is_clearance == True)

    if subcategory_id is not None:
        query = query.where(Product.subcategory_id == subcategory_id)

    if name and len(name) >= 2:  # Убедитесь, что строка поиска содержит хотя бы 2 символа
        query = query.where(Product.name.ilike(f"{name}%"))

    # Используем модифицированный запрос query для подсчета и выборки
    total_query = select(func.count()).select_from(query.subquery())
    items_query = query.offset(offset).limit(page_size).options(
        selectinload(Product.color_1),
        selectinload(Product.color_4),
        selectinload(Product.subcategory)
    )

    total_result = await session.execute(total_query)
    total = total_result.scalar_one()

    items_result = await session.execute(items_query)
    items = items_result.scalars().all()

    return ProductPagination(
        items=[ProductSchema.from_orm(item) for item in items],
        total=total,
        page=page,
        page_size=page_size
    )