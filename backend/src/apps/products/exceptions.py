from fastapi import HTTPException, status


class ProductNotFoundException(HTTPException):
    """Исключение, возникающее при отсутствии продукта."""

    def __init__(self):
        super().__init__(status_code=status.HTTP_404_NOT_FOUND, detail="Продукт не найден")


class SubcategoryNotFoundException(HTTPException):
    """Исключение, возникающее при отсутствии подкатегории."""

    def __init__(self):
        super().__init__(status_code=status.HTTP_404_NOT_FOUND, detail="Подкатегория не найдена")
