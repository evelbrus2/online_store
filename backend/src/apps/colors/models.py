from sqlalchemy import Column, Integer, String, ForeignKey
from config.database import Base
from sqlalchemy.orm import relationship

from fastapi_storages.integrations.sqlalchemy import FileType
from fastapi_storages import FileSystemStorage

# Создаем хранилище для файлов на локальном диске
storage = FileSystemStorage(path="/home/valentin/Desktop/FastApi/FAOS3/Canada/canada/backend/src/apps/images/uploads")

class Image(Base):
    __tablename__ = 'image'
    id = Column(Integer, primary_key=True, index=True)
    image = Column(FileType(storage=storage))  # Изменяем тип поля на FileType и передаем storage


class BaseColor(Base):
    __tablename__ = 'base_color'
    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, unique=True, index=True)
    html_color = Column(String, unique=True, index=True)

    # products_color_1 = relationship("Product", foreign_keys="Product.color_1_id")
    # products_color_2 = relationship("Product", foreign_keys="Product.color_2_id")
    # products_color_3 = relationship("Product", foreign_keys="Product.color_3_id")
    # products_color_4 = relationship("Product", foreign_keys="Product.color_4_id")
    # products_color_5 = relationship("Product", foreign_keys="Product.color_5_id")
    # products_color_6 = relationship("Product", foreign_keys="Product.color_6_id")
    # products_color_7 = relationship("Product", foreign_keys="Product.color_7_id")
    # products_color_8 = relationship("Product", foreign_keys="Product.color_8_id")
    # products_color_9 = relationship("Product", foreign_keys="Product.color_9_id")


    def __str__(self):
        return f"{self.name}"

    class Config:
        orm_mode = True
