from typing import Optional

from pydantic import BaseModel


class BaseColorSchema(BaseModel):
    id: Optional[int] = None
    name: Optional[str] = None
    html_color: Optional[str] | None

    class Config:
        from_attributes = True


class ImageSchema(BaseModel):
    id: int
    image: str | None

    class Config:
        from_attributes = True
