import logging

from .exceptions import ImageNotFoundException, ColorNotFoundException
from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.ext.asyncio import AsyncSession
from config.database import get_db
from .schemas import BaseColorSchema, ImageSchema
from .dao import ContentDAO, ImageDAO, BaseColorDAO

router = APIRouter(
    prefix="/Color",
    tags=["Цвет"]
)

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)

# @router.get("/images/", response_model=list[ImageSchema])
# async def get_all_images(session: AsyncSession = Depends(get_db)):
#     image_dao = ImageDAO(session)
#     images = await image_dao.get_all_images()
#     return images
#
# @router.get("/images/{image_id}/", response_model=ImageSchema)
# async def get_image_by_id(image_id: int, session: AsyncSession = Depends(get_db)):
#     image_dao = ImageDAO(session)
#     try:
#         image = await image_dao.get_image_by_id(image_id)
#         return image
#     except ImageNotFoundException:
#         raise HTTPException(status_code=404, detail="Изображение не найдено")

@router.get("/", response_model=list[BaseColorSchema])
async def get_all_base_colors(session: AsyncSession = Depends(get_db)):
    base_color_dao = BaseColorDAO(session)
    base_colors = await base_color_dao.get_all_base_colors()
    return base_colors

@router.get("/{base_color_id}/", response_model=BaseColorSchema)
async def get_base_color_by_id(base_color_id: int, session: AsyncSession = Depends(get_db)):
    base_color_dao = BaseColorDAO(session)
    try:
        base_color = await base_color_dao.get_base_color_by_id(base_color_id)
        return base_color
    except ColorNotFoundException:
        raise HTTPException(status_code=404, detail="Базовый цвет не найден")