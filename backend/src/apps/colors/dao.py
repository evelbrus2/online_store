from .schemas import BaseColorSchema, ImageSchema
from .exceptions import ColorNotFoundException, ImageNotFoundException
from sqlalchemy.exc import NoResultFound
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.future import select
from sqlalchemy.orm import joinedload, selectinload
from .models import Image, BaseColor
import logging

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)


class ContentDAO:
    def __init__(self, session: AsyncSession):
        self.session = session

class ImageDAO:
    def __init__(self, session: AsyncSession):
        self.session = session

    async def get_all_images(self) -> list[Image]:
        query = select(Image)
        result = await self.session.execute(query)
        images = result.scalars().all()
        return images

    async def get_image_by_id(self, image_id: int) -> Image:
        query = select(Image).where(Image.id == image_id)
        result = await self.session.execute(query)
        image = result.scalar_one_or_none()
        if image is None:
            raise ImageNotFoundException()
        return image


class BaseColorDAO:
    def __init__(self, session: AsyncSession):
        self.session = session

    async def get_all_base_colors(self) -> list[BaseColor]:
        query = select(BaseColor)
        result = await self.session.execute(query)
        base_colors = result.scalars().all()
        return base_colors

    async def get_base_color_by_id(self, base_color_id: int) -> BaseColor:
        query = select(BaseColor).where(BaseColor.id == base_color_id)
        result = await self.session.execute(query)
        base_color = result.scalar_one_or_none()
        if base_color is None:
            raise ColorNotFoundException()
        return base_color
