from .celery_app import celery
from .email_utils import send_email
import os

@celery.task
def send_email_task(to_email):
    subject = "Добро пожаловать"
    # Получаем текущую директорию, где находится данный файл
    current_directory = os.path.dirname(os.path.abspath(__file__))
    # Составляем путь к файлу относительно текущей директории
    html_file_path = os.path.join(current_directory, "rasylka.html")
    # Чтение HTML-контента из файла
    with open(html_file_path, "r", encoding="utf-8") as file:
        html_content = file.read()

    send_email(to_email, subject, html_content, html=True)
