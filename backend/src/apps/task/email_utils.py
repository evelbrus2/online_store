import smtplib
from email.message import EmailMessage
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart


def send_email(to_email, subject, body, html=False):
    sender_email = "evelbrus55@gmail.com"  # Замените на ваш Gmail адрес
    sender_password = "gtpmdktmcdrxhudt"  # Замените на ваш пароль для приложений

    # Создание MIME объекта
    message = EmailMessage()
    message['Subject'] = subject
    message["From"] = sender_email
    message["To"] = to_email

    if html:
        message.set_content(body, subtype='html')  # Установка HTML-контента
    else:
        message.set_content(body)

    # Настройка сервера SMTP
    try:
        server = smtplib.SMTP("smtp.gmail.com", 587)
        server.starttls()  # Защита соединения с помощью TLS
        server.login(sender_email, sender_password)

        # Отправка письма
        server.sendmail(sender_email, to_email, message.as_string())
        print("Email успешно отправлен!")

    except smtplib.SMTPAuthenticationError:
        print("Ошибка аутентификации. Проверьте свой пароль для приложений.")
    except Exception as e:
        print(f"Произошла ошибка при отправке электронной почты: {e}")
    finally:
        server.quit()
