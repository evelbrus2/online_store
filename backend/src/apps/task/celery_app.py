# celery_app.py
from celery import Celery
import os

# Получите текущую директорию
current_directory = os.path.dirname(os.path.abspath(__file__))

celery = Celery(
    "task",
    broker="redis://localhost:6379/0",
    backend="redis://localhost:6379/0",
    include=["src.apps.task.tasks"]
)

celery.conf.broker_connection_retry_on_startup = True

if __name__ == "__main__":
    celery.start()
