from typing import Optional

from pydantic import BaseModel, EmailStr, Field


class UserCreate(BaseModel):
    email: EmailStr
    password: str
    confirm_password: str

    class Config:
        from_attributes = True


class UserResponse(BaseModel):
    user_id: int
    email: str

    class Config:
        from_attributes = True


class UserProfileUpdateRequest(BaseModel):
    first_name: Optional[str] = Field(example="Иван")
    last_name: Optional[str] = Field(example="Иванов")
    display_name: Optional[str] = Field(example="Vanya")

    class Config:
        from_attributes = True


class UserProfileResponse(BaseModel):
    user_id: int
    email: EmailStr
    first_name: str | None
    last_name: str | None
    display_name: str | None

    class Config:
        from_attributes = True


class UserListResponse(BaseModel):
    user_id: int
    email: EmailStr
    first_name: Optional[str] = Field(None, example="Иван")
    last_name: Optional[str] = Field(None, example="Иванов")
    display_name: Optional[str] = Field(None, example="Vanya")
    is_active: bool
    is_superuser: bool
    is_staff: bool

    class Config:
        from_attributes = True


class LoginRequest(BaseModel):
    email: EmailStr = Field(..., title="Email Address")
    password: str = Field(..., title="Password")

    class Config:
        from_attributes = True


class UserChangePasswordRequest(BaseModel):
    old_password: str
    new_password: str
    new_confirm_password: str
