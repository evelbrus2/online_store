from sqlalchemy import Column, Integer, String, Boolean, DateTime
from sqlalchemy.orm import relationship, declarative_base
from datetime import datetime

from config.database import Base


class Users(Base):
    __tablename__ = 'users'
    user_id = Column(Integer, primary_key=True, autoincrement=True)
    email = Column(String, nullable=False)
    hashed_password = Column(String, nullable=False)
    refresh_token = Column(String, index=True, nullable=True)
    is_active = Column(Boolean, default=True)
    is_superuser = Column(Boolean, default=False)
    is_staff = Column(Boolean, default=False)
    first_name = Column(String, nullable=True)
    last_name = Column(String, nullable=True)
    display_name = Column(String, nullable=True)

    created_at = Column(DateTime, default=datetime.utcnow)

    carts = relationship("Cart", back_populates="user", uselist=False)

    def __str__(self):
        return f"{self.email}"

    class Config:
        orm_mode = True
