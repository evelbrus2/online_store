from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.future import select
from fastapi.exceptions import HTTPException

from apps.base.base import BaseDAO
from .models import Users


class UsersDAO(BaseDAO):
    model = Users

    @classmethod
    async def update_user(cls, update_data, current_user, db: AsyncSession):
        stmt = select(Users).where(Users.user_id == current_user.user_id)
        result = await db.execute(stmt)
        user_profile = result.scalars().first()

        if not user_profile:
            raise HTTPException(status_code=404, detail="Профиль пользователя не найден.")

        user_profile.first_name = update_data.first_name or user_profile.first_name
        user_profile.last_name = update_data.last_name or user_profile.last_name

        db.add(user_profile)
        await db.commit()

        return user_profile