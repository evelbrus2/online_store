import json
import logging

from typing import List
from fastapi import APIRouter, Query
from fastapi import Depends, Security
from sqlalchemy.future import select
from sqlalchemy.ext.asyncio import AsyncSession
from fastapi.exceptions import HTTPException

from apps.authorize.dependencies import get_current_user, is_superuser_or_staff
from config.database import get_db
from .dao import UsersDAO
from .models import Users
from .schemas import UserResponse, UserProfileUpdateRequest, UserProfileResponse, UserListResponse

# Настройте логгирование
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

router = APIRouter(
    prefix="/users",
    tags=["Пользователи"]
)


@router.get("/me", response_model=UserProfileResponse)
async def get_user(
        current_user: Users = Depends(get_current_user),
        db: AsyncSession = Depends(get_db)
):
    logger.info(" Запрос о информации пользователя.")
    print(f"Текущий пользователь: {current_user.email}")
    user = await UsersDAO.get_by_id(current_user.user_id, db=db)
    return user


@router.get("/users", response_model=List[UserListResponse])
async def get_users(
        is_authorized: bool = Security(is_superuser_or_staff),
        page: int = Query(default=1, description="Номер страницы"),
        page_size: int = Query(default=10, description="Размер страницы"),
        db: AsyncSession = Depends(get_db)
):
    logger.info(" Запрос со списком пользователей")

    users = await UsersDAO.get_in_page(page=page, page_size=page_size, db=db)
    return users


@router.patch("/me", response_model=UserProfileResponse)
async def update_user(
        request: UserProfileUpdateRequest,
        current_user: Users = Depends(get_current_user),
        db: AsyncSession = Depends(get_db)
):
    logger.info(" Запрос на обновление информации пользователя.")

    updated_user = await UsersDAO.update_user(
        update_data=request,
        current_user=current_user,
        db=db
        )

    return updated_user
