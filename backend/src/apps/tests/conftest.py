import asyncio
import json
import logging
import pytest
from config.config import settings
from config.database import Base, async_session_maker, engine
from apps.categories.models import Category
from apps.subcategery.models import Subcategory
from sqlalchemy import insert

# Настройка логирования
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

@pytest.fixture(scope="session", autouse=True)
@pytest.mark.asyncio
async def prepare_database():
    logger.info("Начало инициализации базы данных")

    assert settings.MODE == "TEST"

    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.drop_all)
        await conn.run_sync(Base.metadata.create_all)

    logger.info("База данных создана")

    def open_mock_json(model: str):
        with open(f"{settings.TEST_DATA_PATH}/mock_{model}.json", "r", encoding="utf-8") as file:
            return json.load(file)

    category = open_mock_json("category")
    logger.info(f"Загруженные данные категории: {category}")

    subcategory = open_mock_json("subcategory")
    logger.info(f"Загруженные данные подкатегории: {subcategory}")

    async with async_session_maker() as session:
        add_category = insert(Category).values(category)
        add_subcategory = insert(Subcategory).values(subcategory)

        await session.execute(add_category)
        logger.info("Категории добавлены в базу данных")
        await session.execute(add_subcategory)
        logger.info("Подкатегории добавлены в базу данных")

        await session.commit()
        logger.info("Изменения зафиксированы в базе данных")

    logger.info("Данные добавлены в базу данных")



