import pytest
from config.database import async_session_maker
from httpx import AsyncClient
from main import app  # Импортируйте ваш FastAPI приложение
from sqlalchemy import text
import logging

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

def test_abc():
    assert 1 == 1


@pytest.mark.asyncio
async def test_async_example():
    # Проверка инициализации базы данных
    async with async_session_maker() as session:
        result = await session.execute(text("SELECT * FROM categories;"))
        categories = result.fetchall()
        logger.info(f"Результат запроса к таблице categories: {categories}")
        assert len(categories) == 0

    # Выполнение запроса к API
    async with AsyncClient(app=app, base_url="http://test") as ac:
        response = await ac.get("/categories/")  # Предполагается, что у вас есть такой эндпоинт
        assert response.status_code == 200
        assert len(response.json()) > 0  # Проверяем, что ответ содержит данные о категориях

