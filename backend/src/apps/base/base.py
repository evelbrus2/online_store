from config.database import AsyncSession
from sqlalchemy import select


class BaseDAO:
    model = None

    @classmethod
    async def get_by_id(cls, id, db: AsyncSession):
        result = await db.execute(select(cls.model).where(cls.model.user_id == id))
        return result.scalars().first()

    @classmethod
    async def get_all(cls, db: AsyncSession, skip: int = 0, limit: int = 2):
        result = await db.execute(select(cls.model).offset(skip).limit(limit))
        return result.scalars().all()

    @classmethod
    async def create(cls, data: dict, db: AsyncSession):
        query = cls.model(**data)
        db.add(query)
        await db.commit()
        await db.refresh(query)
        return query

    @classmethod
    async def update(cls, update_data, db: AsyncSession):
        item = await cls.get_by_id(update_data['id'], db=db)
        for key, value in update_data.items():
            setattr(item, key, value)

        db.add(item)
        await db.commit()

        return item

    @classmethod
    async def get_in_page(cls, page:int, page_size:int, db: AsyncSession):
        async with db as session:
            start_idx = (page - 1) * page_size
            end_idx = start_idx + page_size

            result = await session.execute(select(cls.model))
            items = result.scalars().all()
            items_on_page = items[start_idx:end_idx]

            return items_on_page
