import stripe
from fastapi import APIRouter

stripe.api_key = 'wrinwiurnpriucnwrikjcnr89hu3oin4jfmoifji03-809=2058'

router = APIRouter(
    prefix="/pay",
    tags=["База"]
)

@router.post("/process-payment/")
async def process_payment(amount: int, currency: str, token: str):
    try:
        # Create a charge using the Stripe API
        charge = stripe.Charge.create(
            amount=amount,
            currency=currency,
            source=token,  # Stripe token obtained from the client-side (e.g., Stripe.js)
            description="Payment for FastAPI Store",  # Add a description for the payment
        )

        # You can handle the charge object as per your requirements
        # For example, log the payment or perform other actions

        # Return a success response
        return {"status": "success", "charge_id": charge.id}

    except stripe.error.CardError as e:
        # Handle specific Stripe errors
        return {"status": "error", "message": str(e)}
    except stripe.error.StripeError as e:
        # Handle generic Stripe errors
        return {"status": "error", "message": "Something went wrong. Please try again later."}