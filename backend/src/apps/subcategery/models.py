from sqlalchemy import Column, Integer, ForeignKey, String
from sqlalchemy.orm import relationship

from config.database import Base


class Subcategory(Base):
    __tablename__ = "subcategories"
    id = Column(Integer, primary_key=True, index=True)

    category_id = Column(Integer, ForeignKey("categories.id"))


    name = Column(String, index=True, unique=True, nullable=False)

    product = relationship("Product", back_populates="subcategory")
    category = relationship("Category", back_populates="subcategories")

    def __str__(self):
        return f"{self.name}"

    class Config:
        orm_mode = True
