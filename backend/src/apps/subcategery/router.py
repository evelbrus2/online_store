import logging

from fastapi import APIRouter, Depends, HTTPException, status, Response, Security
from sqlalchemy.ext.asyncio import AsyncSession
from typing import List
from sqlalchemy.exc import IntegrityError
from sqlalchemy.future import select
from sqlalchemy.orm import exc

from config.database import get_db
from apps.authorize.dependencies import get_current_user, is_superuser_or_staff
from apps.users.models import Users
from .schemas import SubcategoryBase, SubcategoryCreate, SubcategoryUpdate, SubcategoryPagination
from .dao import SubcategoryDAO
from apps.categories.models import Category
from .models import Subcategory

# Настройте логгирование
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

router = APIRouter(
    prefix="/subcategory",
    tags=["Подкатегории"]
)


@router.get("/subcategories", response_model=SubcategoryPagination)  # Обновление модели ответа
async def get_subcategories(
    page: int = 1,
    page_size: int = 10,
    db: AsyncSession = Depends(get_db)
):
    """
    Запрос списка подкатегорий с пагинацией.
    """
    try:
        logger.info("Запрос списка подкатегорий с пагинацией.")
        subcategories = await SubcategoryDAO.get_subcategories(db, page=page, page_size=page_size)
        return subcategories
    except Exception as e:
        logger.error(f"Ошибка при получении списка подкатегорий: {e}")
        raise HTTPException(status_code=500, detail=str(e))

@router.get("/{subcategory_id}", response_model=SubcategoryBase)
async def get_subcategory(subcategory_id: int, db: AsyncSession = Depends(get_db)):
    """
    Запрос на определенную подкатегорию.
    """
    logger.info(" Запрос на определенную подкатегорию.")
    subcategory = await SubcategoryDAO.get_subcategory(subcategory_id, db)
    if subcategory is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Подкатегория не найдена.")
    return subcategory

# @router.post("/", response_model=SubcategoryBase, status_code=status.HTTP_201_CREATED)
# async def create_subcategory(subcategory_data: SubcategoryCreate,
#                              current_user: Users = Depends(get_current_user),
#                              is_authorized: bool = Security(is_superuser_or_staff),
#                              db: AsyncSession = Depends(get_db)):
#     """
#     Запрос на создание подкатегории.
#     """
#     logger.info(" Запрос на создание подкатегории.")
#     # Проверка наличия категории в базе данных
#     category_id = subcategory_data.category_id
#     result = await db.execute(select(Category).where(Category.id == category_id))
#     category = result.scalars().first()
#     if not category:
#         raise HTTPException(
#             status_code=status.HTTP_400_BAD_REQUEST,
#             detail=f"Категория с ID {category_id} не существует в таблице 'categories'."
#         )
#
#     try:
#         # Теперь пытаемся создать подкатегорию
#         return await SubcategoryDAO.create_subcategory(subcategory_data.dict(), db)
#     except IntegrityError as e:
#         await db.rollback()
#         error_detail = str(e.orig)
#         if 'UniqueViolationError' in error_detail:
#             raise HTTPException(
#                 status_code=status.HTTP_400_BAD_REQUEST,
#                 detail=f"Подкатегория с именем '{subcategory_data.name}' уже существует."
#             )
#         else:
#             raise HTTPException(
#                 status_code=status.HTTP_400_BAD_REQUEST,
#                 detail="Произошла неизвестная ошибка при создании подкатегории."
#             )


# @router.patch("/{subcategory_id}", response_model=SubcategoryBase)
# async def update_subcategory(subcategory_id: int, subcategory_data: SubcategoryUpdate,
#                              current_user: Users = Depends(get_current_user),
#                              is_authorized: bool = Security(is_superuser_or_staff),
#                              db: AsyncSession = Depends(get_db)):
#     """
#     Запрос на редактирование подкатегории.
#     """
#     logger.info(" Запрос на редактирование подкатегории.")
#     # Проверяем, существует ли подкатегория
#     subcategory = await db.get(Subcategory, subcategory_id)
#     if not subcategory:
#         raise HTTPException(
#             status_code=status.HTTP_404_NOT_FOUND,
#             detail="Подкатегория не найдена."
#         )

#     # Проверяем, существует ли категория, если id категории обновляется
#     if 'category_id' in subcategory_data.dict() and subcategory_data.category_id is not None:
#         category = await db.get(Category, subcategory_data.category_id)
#         if not category:
#             raise HTTPException(
#                 status_code=status.HTTP_400_BAD_REQUEST,
#                 detail="Категория с указанным ID не существует."
#             )
#
#     # Обновляем подкатегорию
#     for key, value in subcategory_data.dict().items():
#         if value is not None:
#             setattr(subcategory, key, value)
#
#     db.add(subcategory)
#     try:
#         await db.commit()
#         await db.refresh(subcategory)
#         return subcategory
#     except IntegrityError as e:
#         await db.rollback()
#         error_detail = str(e.orig)
#         if 'UniqueViolationError' in error_detail:
#             raise HTTPException(
#                 status_code=status.HTTP_400_BAD_REQUEST,
#                 detail="Подкатегория с таким именем уже существует."
#             )
#         else:
#             raise HTTPException(
#                 status_code=status.HTTP_400_BAD_REQUEST,
#                 detail="Произошла ошибка при обновлении подкатегории."
#             )
#
#
# @router.delete("/{subcategory_id}", status_code=status.HTTP_204_NO_CONTENT)
# async def delete_subcategory(subcategory_id: int,
#                              current_user: Users = Depends(get_current_user),
#                              is_authorized: bool = Security(is_superuser_or_staff),
#                              db: AsyncSession = Depends(get_db)):
#     """
#     Запрос на удаление подкатегории.
#     """
#     logger.info(" Запрос на удаление подкатегории.")
#     # Проверяем, существует ли подкатегория
#     query = select(Subcategory).where(Subcategory.id == subcategory_id)
#     result = await db.execute(query)
#     instance = result.scalars().first()
#     if not instance:
#         raise HTTPException(
#             status_code=status.HTTP_404_NOT_FOUND,
#             detail="Подкатегория не найдена."
#         )
#
#     try:
#         # Если подкатегория существует, удаляем её
#         await SubcategoryDAO.delete_subcategory(subcategory_id, db)
#     except exc.NoResultFound:
#         # Если подкатегория уже была удалена или не существовала, возвращаем 404
#         raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Подкатегория не найдена.")
#     except Exception as e:  # Здесь лучше использовать более специфичный тип исключения
#         # Если произошла другая ошибка, возвращаем 400
#         raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(e))
#
#     return Response(status_code=status.HTTP_204_NO_CONTENT)
