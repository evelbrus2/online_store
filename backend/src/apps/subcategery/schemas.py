from pydantic import BaseModel
from typing import Optional, List


class SubcategoryBase(BaseModel):
    id: Optional[int]
    name: Optional[str]
    category_id: Optional[int] = None  # Change the type to Optional[int]

    class Config:
        from_attributes = True


class SubcategoryCreate(BaseModel):
    name: str
    category_id: int

    class Config:
        from_attributes = True


class SubcategoryUpdate(BaseModel):
    name: str = None
    category_id: int = None

    class Config:
        from_attributes = True


class SubcategorySchema(BaseModel):
    id: int
    name: str  # Предположим, что у Subcategory есть атрибут name

    class Config:
        from_attributes = True


class SubcategoryPagination(BaseModel):
    items: List[SubcategoryBase]
    total: int
    page: int
    page_size: int

    class Config:
        from_attributes = True