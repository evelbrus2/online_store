from .schemas import SubcategoryPagination
from sqlalchemy import func
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.future import select

from .models import Subcategory


class SubcategoryDAO:
    @classmethod
    async def get_subcategories(cls, db: AsyncSession, page: int, page_size: int):
        offset = (page - 1) * page_size
        query = select(Subcategory).offset(offset).limit(page_size)
        total_query = select(func.count(Subcategory.id))

        total_result = await db.execute(total_query)
        total = total_result.scalar_one()

        result = await db.execute(query)
        subcategories = result.scalars().all()

        return SubcategoryPagination(
            items=subcategories,
            total=total,
            page=page,
            page_size=page_size,
        )

    @classmethod
    async def create_subcategory(cls, subcategory_data: dict, db: AsyncSession):
        subcategory = Subcategory(**subcategory_data)
        db.add(subcategory)
        await db.commit()
        await db.refresh(subcategory)
        return subcategory

    @classmethod
    async def get_subcategory(cls, subcategory_id: int, db: AsyncSession):
        result = await db.execute(select(Subcategory).where(Subcategory.id == subcategory_id))
        return result.scalars().first()

    @classmethod
    async def update_subcategory(cls, subcategory_id: int, subcategory_data: dict, db: AsyncSession):
        result = await db.execute(select(Subcategory).where(Subcategory.id == subcategory_id))
        subcategory = result.scalars().first()
        if subcategory:
            for key, value in subcategory_data.items():
                setattr(subcategory, key, value)
            await db.commit()
            await db.refresh(subcategory)
            return subcategory
        return None

    @classmethod
    async def delete_subcategory(cls, subcategory_id: int, db: AsyncSession):
        result = await db.execute(select(Subcategory).where(Subcategory.id == subcategory_id))
        subcategory = result.scalars().first()
        if subcategory:
            await db.delete(subcategory)
            await db.commit()
            return True
        return False
