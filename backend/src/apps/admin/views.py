from sqladmin import ModelView, BaseView, expose
from sqlalchemy import select
from sqlalchemy.orm import Session
from sqlalchemy.sql.functions import current_user, func

from apps.users.models import Users
from apps.categories.models import Category
from apps.subcategery.models import Subcategory
from apps.products.models import Product
from apps.colors.models import Image, BaseColor
from apps.basket.models import Cart, CartProduct
from sqlalchemy.testing.pickleable import User
from wtforms import StringField



class UsersAdmin(ModelView, model=Users):
    column_list = [
                      Users.email,
                  ] + [Users.is_active]
    column_labels = {
        Users.email: "Email",
        Users.is_active: "Активен"
    }
    column_details_exclude_list = [Users.user_id, Users.hashed_password, Users.refresh_token]
    page_size = 50
    can_delete = False

    name = "Пользователь"
    name_plural = "Пользователи"
    icon = "fa-solid fa-user"



class CategoryAdmin(ModelView, model=Category):
    column_list = [
        Category.name,
                  ] + [Category.subcategories]
    column_labels = {
        Category.id: "Категории",
        Category.name: "Категория",
    }

    column_details_exclude_list = [Category.id, Category.subcategories]
    form_columns = ['name']
    page_size = 50

    name = "Категории"
    name_plural = "Категории"
    icon = "fa-solid fa-folder"


class SubcategoryAdmin(ModelView, model=Subcategory):
    column_list = [
        Subcategory.name,
                  ] + [Subcategory.category]
    column_labels = {
        Subcategory.id: "Подкатегории",
        Subcategory.name: "Название подкатегорий",
        Subcategory.category: "Принадлежит Категории",
    }

    column_details_exclude_list = [Subcategory.id, Subcategory.category_id, Subcategory.product]
    form_columns = ['name', 'category']
    page_size = 50
    #edit_template = "edit_template.html"

    name = "Подкатегории"
    name_plural = "Подкатегории"
    icon = "fa-solid fa-folder"

from wtforms import MultipleFileField
from fastapi import UploadFile

class ProductsAdmin(ModelView, model=Product):

    async def _handle_form_data(self, request, model):
        form_data = []
        form = await request.form()
        for key, value in form.multi_items():
            if isinstance(value, list):  # Если значение является списком
                for file_item in value:
                    if isinstance(file_item, UploadFile):
                        form_data.append((key, file_item))
                    else:
                        # Если элемент списка не является объектом UploadFile, здесь можно добавить дополнительную обработку
                        pass
            else:
                form_data.append((key, value))
        return form_data

    column_list = [
        Product.name,
        Product.description,
        Product.price,
        Product.manufacturer,
        Product.is_best_seller,
        Product.is_clearance,
    ] + [Product.subcategory]
    # column_details_exclude_list = [Product.id, Product.cart_products, Product.subcategories_id]
    column_labels = {
        Product.id: "Товары",
        Product.name: "Название товара",
        Product.description: "Описание товара",
        Product.price: "Цена",
        Product.length: "Длина",
        Product.width: "Ширина",
        Product.height: "Высота",
        Product.manufacturer: "Производитель",
        Product.is_best_seller: "топ-продажа",
        Product.is_clearance: "распродажа",
        Product.subcategory: "Подкатегория",
        Product.color_1: "Цвет 1",
        Product.color_2: "Цвет 2",
        Product.color_3: "Цвет 3",
        Product.color_4: "Цвет 4",
        Product.color_5: "Цвет 5",
        Product.color_6: "Цвет 6",
        Product.color_7: "Цвет 7",
        Product.color_8: "Цвет 8",
        Product.color_9: "Цвет 9",
        Product.images_1: "Изображения относящиеся к первого цвету",
        Product.images_2: "Изображения относящиеся к второму цвету",
        Product.images_3: "Изображения относящиеся к третьему цвету",
        Product.images_4: "Изображения относящиеся к четвертному цвету",
        Product.images_5: "Изображения относящиеся к пятому цвету",
        Product.images_6: "Изображения относящиеся к шестому цвету",
        Product.images_7: "Изображения относящиеся к седьмому цвету",
        Product.images_8: "Изображения относящиеся к восьмому цвету цвету",
        Product.images_9: "Изображения относящиеся к девятому цвету цвету",
        Product.article_1: "Артикль относящиеся к первому цвету товара",
        Product.article_2: "Артикль относящиеся к второму цвету товара",
        Product.article_3: "Артикль относящиеся к третьему цвету товара",
        Product.article_4: "Артикль относящиеся к четвертному цвету товара",
        Product.article_5: "Артикль относящиеся к пятому цвету товара",
        Product.article_6: "Артикль относящиеся к шестому цвету товара",
        Product.article_7: "Артикль относящиеся к седьмому цвету товара",
        Product.article_8: "Артикль относящиеся к восьмому цвету товара",
        Product.article_9: "Артикль относящиеся к девятому цвету товара"


    }

    form_overrides = {
        "images_1": MultipleFileField,
        "images_2": MultipleFileField,
        "images_3": MultipleFileField,
        "images_4": MultipleFileField,
        "images_5": MultipleFileField,
        "images_6": MultipleFileField,
        "images_7": MultipleFileField,
        "images_8": MultipleFileField,
        "images_9": MultipleFileField,

    }

    form_args = {
        "images_10": {
            "label": "Images 10",
            "validators": []  # Можно добавить валидаторы, если необходимо
        }
    }


    page_size = 50

    name = "Товары"
    name_plural = "Товары"
    icon = "fa-solid fa-shopping-bag"

    #column_details_exclude_list = [Product.id, Product.cart_products]
    column_searchable_list = ['name']
    column_sortable_list = ['id', 'name', 'description', 'price', 'length', 'width', 'height', 'manufacturer', 'is_best_seller', 'is_clearance']
    column_filters = ['name', 'description', 'price', 'length', 'width', 'height', 'manufacturer''is_best_seller', 'is_clearance']
    form_columns = [
        'id',
        'name',
        "subcategory",
        'description',
        'price',
        'length',
        'width',
        'height',
        'manufacturer',
        'color_1',
        'images_1',
        'color_2',
        'images_2',
        'color_3',
        'images_3',
        'color_4',
        'images_4',
        'color_5',
        'images_5',
        'color_6',
        'images_6',
        'color_7',
        'images_7',
        'color_8',
        'images_8',
        'color_9',
        'images_9',
        'is_best_seller',
        'is_clearance',
    ]
    column_details_list = [
        'name',
        "subcategory",
        'description',
        'price',
        'length',
        'width',
        'height',
        'manufacturer',
        'color_1',
        'images_1',
        'article_1',
        'color_2',
        'images_2',
        'article_2',
        'color_3',
        'images_3',
        'article_3',
        'color_4',
        'images_4',
        'article_4',
        'color_5',
        'images_5',
        'article_5',
        'color_6',
        'images_6',
        'article_6',
        'color_7',
        'images_7',
        'article_7',
        'color_8',
        'images_8',
        'article_8',
        'color_9',
        'images_9',
        'article_9',
        'is_best_seller',
        'is_clearance',
    ]

class BasketAdmin(ModelView, model=Cart):
    column_list = [
                      c.name for c in Cart.__table__.c
                  ] + [Cart.user]
    column_details_exclude_list = [Cart.id]
    page_size = 50

    name = "Корзина"
    name_plural = "Корзина"
    icon = "fa-solid fa-shopping-bag"


class CartProductAdmin(ModelView, model=CartProduct):
    column_list = [
                      c.name for c in CartProduct.__table__.c
                  ] + [CartProduct.product]
    column_details_exclude_list = [CartProduct.id]
    page_size = 50

    name = "Товары в корзине"
    name_plural = "Товары в корзине"
    icon = "fa-solid fa-shopping-bag"


import logging

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)

class BaseColor(ModelView, model=BaseColor):
    column_list = [
        'id',
        'name',
    ]
    column_details_exclude_list = [BaseColor.id]

    page_size = 50
    can_delete = False
    form_columns = ['id', 'name', 'html_color']

    name = "Базовый цвет"
    name_plural = "Базовый цвет"
    icon = "fa-solid fa-palette"

