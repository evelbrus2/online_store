
from apps.authorize.auth import create_access_token
from apps.authorize.dependencies import get_current_user, authenticate_user, decode_token, get_user_by_email

from sqladmin.authentication import AuthenticationBackend

from starlette.requests import Request



class AdminAuth(AuthenticationBackend):
    async def login(self, request: Request) -> bool:
        form = await request.form()
        email, password = form["username"], form["password"]
        user = await authenticate_user(email, password, request.state.db)
        if user:
            access_token = create_access_token(data={"sub": user.email})
            request.session.update({"token": access_token})
            return True
        return False

    async def logout(self, request: Request) -> bool:
        request.session.clear()
        return True

    async def authenticate(self, request: Request) -> bool:
        token = request.session.get("token")
        if not token:
            return False
        try:
            payload = await decode_token(token)
            user = await get_user_by_email(payload.get("sub"), request.state.db)
            if user:
                return True
        except Exception as e:
            # Обрабатываем исключения, связанные с декодированием токена и поиском пользователя
            return False
        return False



authentication_backend = AdminAuth(secret_key="...")