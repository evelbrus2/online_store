# adminpanel.py
from sqladmin import Admin, ModelView
from apps.admin.auto_admin import authentication_backend

from apps.admin.views import UsersAdmin, CategoryAdmin, SubcategoryAdmin, ProductsAdmin, BasketAdmin, \
    CartProductAdmin, BaseColor
from config.database import engine  # Предполагается, что engine уже настроен в database.py
from apps.users.models import Users # Импорт вашей модели пользователей


def setup_admin(app):
    # Создание экземпляра административной панели
    admin = Admin(app, engine, authentication_backend=authentication_backend)

    # Добавление представления модели в админ-панель
    admin.add_view(UsersAdmin)
    admin.add_view(CategoryAdmin)
    admin.add_view(SubcategoryAdmin)
    admin.add_view(ProductsAdmin)
    admin.add_view(BaseColor)
    admin.add_view(BasketAdmin)
    admin.add_view(CartProductAdmin)

    # Возвращаем экземпляр admin, если потребуется дальнейшая настройка в main.py
    return admin
