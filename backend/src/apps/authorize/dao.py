from typing import Optional
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.future import select
from datetime import datetime, timedelta
from fastapi import Response, Depends


from .auth import get_password_hash, verify_password, create_access_token, \
    create_refresh_token, verify_refresh_token, ALGORITHM, SECRET_KEY, ACCESS_TOKEN_EXPIRE_MINUTES, \
    REFRESH_TOKEN_EXPIRE_DAYS
from apps.base.base import BaseDAO
from apps.users.models import Users

from .exceptions import (
    UserAlreadyExistsException, IncorrectCredentialsException, PasswordsDontMatchException,
    InvalidRefreshTokenException, IncorrectOldPasswordException
)


class AuthDAO(BaseDAO):
    model = Users

    @classmethod
    async def find_by_refresh_token(cls, refresh_token: str, db: AsyncSession) -> Optional[Users]:
        result = await db.execute(select(cls.model).where(cls.model.refresh_token == refresh_token))
        return result.scalars().first()

    @classmethod
    async def register(cls, email: str, password: str, confirm_password:str, db: AsyncSession) -> Optional[Users]:
        existing_user = await cls.find_by_email(email=email, db=db)
        if existing_user:
            raise UserAlreadyExistsException()
        if password != confirm_password:
            raise PasswordsDontMatchException()
        hashed_password = get_password_hash(password)
        await cls.create({
            "email": email,
            "hashed_password": hashed_password}, 
            db=db
            )
        return {"message": "Вы зарегестрировались."}

    @classmethod
    async def login(cls, email: str, password: str, response: Response, db: AsyncSession) -> Optional[Users]:
        user = await cls.find_by_email(email=email, db=db)
        if not user or not verify_password(password, user.hashed_password):
            raise IncorrectCredentialsException()
        access_token = create_access_token(data={"sub": user.email})
        access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
        formatted_access_token_expires = (datetime.utcnow() + access_token_expires).strftime(
            "%a, %d-%b-%Y %H:%M:%S GMT")
        response.set_cookie(key="access_token", value=access_token, httponly=False, secure=False, samesite="Lax",
                            expires=formatted_access_token_expires)
        user_refresh_token = create_refresh_token(data={"sub": user.email})
        refresh_token_expires = timedelta(days=REFRESH_TOKEN_EXPIRE_DAYS)
        formatted_refresh_token_expires = (datetime.utcnow() + refresh_token_expires).strftime(
            "%a, %d-%b-%Y %H:%M:%S GMT")
        response.set_cookie(key="refresh_token", value=user_refresh_token, httponly=False, secure=False, samesite="Lax",
                            expires=formatted_refresh_token_expires)
        user.refresh_token = user_refresh_token
        db.add(user)
        await db.commit()
        return {"message": "Вы вошли в систему."}

    @classmethod
    async def find_by_email(cls, email: str, db: AsyncSession):
        result = await db.execute(select(cls.model).where(cls.model.email == email))
        return result.scalars().first()

    @classmethod
    async def find_by_refresh_token(cls, refresh_token: str, db: AsyncSession):
        result = await db.execute(select(cls.model).where(cls.model.refresh_token == refresh_token))
        return result.scalars().first()

    @classmethod
    async def refresh_token(cls, refresh_token: str, response: Response, db:AsyncSession):
        if not verify_refresh_token(refresh_token, SECRET_KEY, ALGORITHM):
            raise InvalidRefreshTokenException()
        access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
        formatted_access_token_expires = (datetime.utcnow() + access_token_expires).strftime(
            "%a, %d-%b-%Y %H:%M:%S GMT")
        user = await cls.find_by_refresh_token(refresh_token=refresh_token, db=db)
        new_access_token = create_access_token(data={"sub": user.email})
        response.set_cookie(key="access_token", value=new_access_token, httponly=False, secure=False, samesite='Lax',
                            expires=formatted_access_token_expires)
        return {"message": "Ваш токен обновлен."}

    @classmethod
    async def change_password(cls, old_password: str, new_password: str,new_confirm_password: str, current_user, db: AsyncSession):
        if not verify_password(old_password, current_user.hashed_password):
            raise IncorrectOldPasswordException()

        if new_password != new_confirm_password:
            raise PasswordsDontMatchException()

        new_password_hash = get_password_hash(new_password)
        current_user.hashed_password = new_password_hash
        await db.commit()
        return {"message": "Пароль успешно изменен."}
