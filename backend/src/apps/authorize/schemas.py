from pydantic import BaseModel


class RefreshTokenRequest(BaseModel):
    refresh_token: str

    class Config:
        from_attributes = True


class TokenResponse(BaseModel):
    access_token: str
    refresh_token: str
    token_type: str = "bearer"

    class Config:
        from_attributes = True


class RefreshTokenResponse(BaseModel):
    access_token: str
    token_type: str = "bearer"

    class Config:
        from_attributes = True


class MessageResponse(BaseModel):
    message: str
