import logging

from sqlalchemy.exc import SQLAlchemyError
from starlette.status import HTTP_201_CREATED
from fastapi import APIRouter, Response, Request
from fastapi import Depends
from sqlalchemy.ext.asyncio import AsyncSession

from config.database import get_db
from config.config import settings

from .dao import AuthDAO
from apps.users.models import Users
from apps.users.schemas import UserChangePasswordRequest, UserCreate, UserResponse, LoginRequest

from .dependencies import get_current_user
from .schemas import RefreshTokenRequest, MessageResponse

from .exceptions import DatabaseConnectionException

log_level = logging.DEBUG if settings.ENV == 'development' else logging.INFO

logging.basicConfig(level=log_level)
logger = logging.getLogger(__name__)


router = APIRouter(
    prefix="/auth",
    tags=["Аутентификация & Авторизация"]
)


@router.post("/register_user", status_code=HTTP_201_CREATED)
async def register_user(user_data: UserCreate, db: AsyncSession = Depends(get_db)):
    """
    Регистрирует нового пользователя.
    """
    logger.debug("-->Запрос на регистрацию в систему получен.")
    try:
        logger.info(f"-->Запрос на регистрацию. \n-->Пользователь {user_data.email} успешно аутентифицирован.")
        return await AuthDAO.register(
            user_data.email, 
            user_data.password, 
            user_data.confirm_password, 
            db=db
            )
    except SQLAlchemyError as e:
        logger.error(f"-->Ошибка подключения к базе данных: {e}")
        raise DatabaseConnectionException()


@router.post("/login_user", response_model=MessageResponse, status_code=HTTP_201_CREATED)
async def login_user(request: LoginRequest, response: Response, db: AsyncSession = Depends(get_db)):
    """
    Аутентифицирует пользователя и возвращает токены.
    """
    try:
        logger.info(f"-->Пользователь {request.email} успешно аутентифицирован.")
        return await AuthDAO.login(
            request.email, 
            request.password,
            response=response, 
            db=db
            )
    except SQLAlchemyError as e:
        logger.error(f"-->Ошибка базы данных при входе пользователя: {e}")
        raise DatabaseConnectionException()


@router.post("/refresh_token", response_model=MessageResponse, status_code=HTTP_201_CREATED)
async def refresh_token(
        request: Request,
        response: Response,
        db: AsyncSession = Depends(get_db)):
    """
    Обновляет токен.
    """
    logger.debug("-->Запрос на обновления токена в систему получен.")
    try:
        logger.info(f"-->Запрос на обновления токена. \n-->Пользователь успешно обновил токен.")
        return await AuthDAO.refresh_token(
            request.cookies.get("refresh_token"),
            response,
            db=db)
    except SQLAlchemyError as e:
        logger.error(f"-->Ошибка базы данных при обновлении токена: {e}")
        raise DatabaseConnectionException()


@router.post("/logout_user", status_code=HTTP_201_CREATED)
async def logout_user(response: Response,
                      current_user: Users = Depends(get_current_user)):
    """
    Позволяет выйти пользователю.
    """
    logger.debug("-->Запрос на выход из системы получен.")
    try:
        logger.info("-->Запрос на выход.")
        response.delete_cookie(key="access_token", httponly=True, secure=True, samesite="None")
        response.delete_cookie(key="refresh_token", httponly=True, secure=True, samesite="None")
        logger.info(f"-->Пользователь {current_user.email} успешно вышел.")
        return {"message": "Вы успешно вышли."}
    except SQLAlchemyError as e:
        logger.error(f"Ошибка базы данных при выходе пользователя: {e}")
        raise DatabaseConnectionException()


@router.post("/change_password", status_code=HTTP_201_CREATED)
async def change_password(
        request: UserChangePasswordRequest,
        current_user: Users = Depends(get_current_user),
        db: AsyncSession = Depends(get_db)
):
    """
    Позволяет изменить пароль текущему пользователю.
    """
    logger.debug("-->Запрос на смену пароля из системы получен.")
    try:
        logger.info("-->Запрос на смену пароля получен.")
        logger.info(f"-->Пользователь {current_user.email} успешно сменил пароль.")
        return await AuthDAO.change_password(
            old_password=request.old_password,
            new_password=request.new_password,
            new_confirm_password=request.new_confirm_password,
            current_user=current_user,
            db=db
            )
    except SQLAlchemyError as e:
        logger.error(f"Ошибка базы данных при изменении пароля: {e}")
        raise DatabaseConnectionException()
