from typing import Optional

from fastapi import Request, Depends, HTTPException, status
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.future import select

from config.database import get_db
from apps.users.models import Users
from .auth import ALGORITHM, SECRET_KEY, verify_password
from jwt import PyJWTError, decode as jwt_decode
from starlette.status import HTTP_401_UNAUTHORIZED, HTTP_403_FORBIDDEN


def get_token_from_cookie(request: Request) -> str:
    token = request.cookies.get("access_token")
    if not token:
        raise HTTPException(status_code=HTTP_401_UNAUTHORIZED, detail="Not authenticated")
    return token


async def decode_token(token: str) -> dict:
    try:
        return jwt_decode(token, SECRET_KEY, algorithms=[ALGORITHM])
    except PyJWTError:
        raise HTTPException(
            status_code=HTTP_401_UNAUTHORIZED,
            detail="Could not validate credentials",
            headers={"WWW-Authenticate": "Bearer"},
        )

async def authenticate_user(email: str, password: str, db: AsyncSession) -> Optional[Users]:
    user = await get_user_by_email(email, db)
    if user and verify_password(password, user.hashed_password):
        return user
    return None

async def get_user_by_email(email: str, db: AsyncSession) -> Users:
    result = await db.execute(select(Users).where(Users.email == email))
    return result.scalars().first()


async def get_current_user(db: AsyncSession = Depends(get_db), token: str = Depends(get_token_from_cookie)) -> Users:
    payload = await decode_token(token)
    email = payload.get("sub")
    if email is None:
        raise HTTPException(status_code=HTTP_401_UNAUTHORIZED, detail="Could not validate credentials")

    user = await get_user_by_email(email, db)
    if user is None:
        raise HTTPException(status_code=HTTP_401_UNAUTHORIZED, detail="User not found")
    return user


def is_superuser_or_staff(current_user: Users = Depends(get_current_user)) -> bool:
    if not (current_user.is_superuser or current_user.is_staff):
        raise HTTPException(
            status_code=HTTP_403_FORBIDDEN,
            detail="The user doesn't have enough privileges"
        )
    return True

