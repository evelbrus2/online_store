from sqlalchemy.future import select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy import update, delete
from sqlalchemy.orm import joinedload

from .schemas import AddToCartRequest
from .models import Cart, CartProduct
# from products.models import Product
from sqlalchemy.exc import SQLAlchemyError


class CartDAO:
    @classmethod
    async def get_cart_items(cls, user_id: int, db: AsyncSession):
        cart_result = await db.execute(
            select(Cart).where(Cart.user_id == user_id)
        )
        user_cart = cart_result.scalars().first()
        if not user_cart:
            return []
        cart_items_result = await db.execute(
            select(CartProduct).options(joinedload(CartProduct.product)).where(CartProduct.cart_id == user_cart.id)
        )
        cart_items = cart_items_result.scalars().all()
        return cart_items

    @classmethod
    async def add_to_cart(cls, user_id: int, item: AddToCartRequest, db: AsyncSession):
        """
        Добавляет товар в корзину пользователя, обновляет его количество,
        или удаляет, если количество равно нулю или меньше.
        """
        # Получаем или создаем корзину пользователя
        cart_result = await db.execute(
            select(Cart).where(Cart.user_id == user_id)
        )
        user_cart = cart_result.scalars().first()

        if not user_cart:
            user_cart = Cart(user_id=user_id)
            db.add(user_cart)
            await db.flush()  # flush для получения id созданной корзины

        # Проверяем, существует ли уже этот продукт в корзине
        existing_product_result = await db.execute(
            select(CartProduct).where(
                CartProduct.cart_id == user_cart.id,
                CartProduct.product_id == item.product_id
            )
        )
        existing_product = existing_product_result.scalars().first()

        # Если продукт существует, обновляем или удаляем его в зависимости от количества
        if existing_product:
            if item.quantity > 0:
                # Обновляем количество, если оно больше нуля
                await db.execute(
                    update(CartProduct).
                    where(CartProduct.id == existing_product.id).
                    values(quantity=item.quantity)
                )
            else:
                # Удаляем продукт из корзины, если количество равно нулю или меньше
                await db.execute(
                    delete(CartProduct).
                    where(CartProduct.id == existing_product.id)
                )
        else:
            # Добавляем продукт в корзину, если он еще не существует и его количество больше нуля
            if item.quantity > 0:
                new_cart_product = CartProduct(cart_id=user_cart.id, product_id=item.product_id, quantity=item.quantity)
                db.add(new_cart_product)

        await db.commit()

    @classmethod
    async def remove_from_cart(cls, user_id: int, db: AsyncSession):
        """
        Удаляет все товары из корзины пользователя или возвращает сообщение, если корзина пуста.
        """
        # Сначала получаем корзину пользователя
        cart_result = await db.execute(
            select(Cart).where(Cart.user_id == user_id)
        )
        user_cart = cart_result.scalars().first()

        # Если у пользователя нет корзины, возвращаем сообщение
        if not user_cart:
            return "Корзина пуста"

        # Проверяем, есть ли товары в корзине
        cart_products_result = await db.execute(
            select(CartProduct).where(CartProduct.cart_id == user_cart.id)
        )
        cart_products = cart_products_result.scalars().all()

        if not cart_products:
            return "Корзина пуста"

        # Удаляем все продукты из корзины
        await db.execute(
            delete(CartProduct).where(CartProduct.cart_id == user_cart.id)
        )
        await db.commit()
        return "Все товары удалены из корзины"
