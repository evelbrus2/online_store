from pydantic import BaseModel
from typing import Optional

from apps.products.schemas import ProductSchemaBasket


# class ProductSchema(BaseModel):
#     id: int
#     name: str
#     price: float
#
#     # Добавьте другие поля, которые могут быть необходимы
#
#     class Config:
#         from_attributes = True


class CartProductSchema(BaseModel):
    id: int
    quantity: int
    product: ProductSchemaBasket  # Вложенная схема для деталей продукта

    class Config:
        from_attributes = True


class AddToCartRequest(BaseModel):
    product_id: int
    quantity: int = 1  # Значение по умолчанию 1

    class Config:
        from_attributes = True
