from sqlalchemy import Column, Integer, ForeignKey
from sqlalchemy.orm import relationship

from config.database import Base



class CartProduct(Base):
    __tablename__ = 'cart_products'
    id = Column(Integer, primary_key=True, autoincrement=True)
    cart_id = Column(Integer, ForeignKey('carts.id'))
    product_id = Column(Integer, ForeignKey('products.id'))
    quantity = Column(Integer, nullable=False, default=1)

    product = relationship("Product", back_populates="cart_products")

    cart = relationship("Cart", back_populates="cart_products")


    def __str__(self):
        return f"Товар № {self.product_id}"

    class Config:
        orm_mode = True


class Cart(Base):
    __tablename__ = 'carts'
    id = Column(Integer, primary_key=True, autoincrement=True)
    user_id = Column(Integer, ForeignKey('users.user_id'), unique=True)

    # Отношение к пользователю
    user = relationship("Users", back_populates="carts")

    cart_products = relationship("CartProduct", back_populates="cart")

    def __str__(self):
        return f"Корзина пользователя № {self.user_id}"

    class Config:
        orm_mode = True

