import logging
from typing import List

from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy import select, exists

from config.database import get_db
from config.config import settings

from apps.authorize.dependencies import get_current_user
from apps.products.models import Product
# from products.models import Product
from apps.users.models import Users

from .schemas import CartProductSchema, AddToCartRequest
from .dao import CartDAO

log_level = logging.DEBUG if settings.ENV == 'development' else logging.INFO

logging.basicConfig(level=log_level)
logger = logging.getLogger(__name__)

router = APIRouter(
    prefix="/basket",
    tags=["Корзина"]
)


@router.get("/cart/items", response_model=List[CartProductSchema])
async def get_cart_items(
        db: AsyncSession = Depends(get_db),
        current_user: Users = Depends(get_current_user)
):
    """
    Возвращает список товаров в корзине текущего пользователя.
    """
    logger.debug("-->Запрос на получение товаров из корзины получен в систему получен.")
    try:
        logger.info("-->Запрос на получение товаров из корзины.")
        cart_items = await CartDAO.get_cart_items(user_id=current_user.user_id, db=db)
        logger.info(f"-->Корзина пользователя {current_user.email} получена.")
        return cart_items
    except Exception as e:
        logger.error(f"-->Ошибка базы данных при попытке получить товары из корзины: {e}")
        raise HTTPException(status_code=400, detail=f"Ошибка при получении элементов корзины: {e}")


@router.post("/cart/add")
async def add_to_cart(
        item: AddToCartRequest,
        db: AsyncSession = Depends(get_db),
        current_user: Users = Depends(get_current_user)
):
    """
    Добавляет товар в корзину пользователя.
    """
    logger.debug("-->Запрос на перемещение товаров в корзину в систему получен.")
    logger.info("-->Запрос на перемещение товаров в корзину.")
    product_exists = await db.scalar(
        select(exists().where(Product.id == item.product_id))
    )
    if not product_exists:
        raise HTTPException(status_code=404, detail="Продукт не найден")
    try:
        await CartDAO.add_to_cart(user_id=current_user.user_id, item=item, db=db)
        return {"message": "Товар добавлен в корзину"}
    except Exception as e:
        logger.error(f"-->Ошибка базы данных при попытке переместить товары в корзину: {e}")
        raise HTTPException(status_code=400, detail=str(e))


@router.delete("/cart/remove")
async def remove_from_cart(
        db: AsyncSession = Depends(get_db),
        current_user: Users = Depends(get_current_user)
):
    """
    Удаляет все товары из корзины пользователя или сообщает, что корзина пуста.
    """
    logger.debug("-->Запрос на удаление товаров из корзину в систему получен.")
    try:
        logger.info("-->Запрос на удаление товаров из корзины.")
        message = await CartDAO.remove_from_cart(user_id=current_user.user_id, db=db)
        return {"message": message}
    except Exception as e:
        logger.error(f"-->Ошибка базы данных при попытке удалить товары из корзины: {e}")
        raise HTTPException(status_code=400, detail=str(e))
