# router.py
from fastapi import APIRouter, Depends, HTTPException, File, UploadFile, Form, Response, status, Security

from apps.task.celery_app import celery
from .schemas import EmailData

router = APIRouter(
    prefix="/email_visa",
    tags=["Почта"]
)


@router.post("/send-email/")
async def send_email_endpoint(email_data: EmailData):
    # Валидация данных в email_data может быть добавлена по необходимости
    to_email = email_data.to_email
    subject = email_data.subject
    body = email_data.body

    if not to_email:
        raise HTTPException(status_code=400, detail="Invalid email_visa address")

    # Отправляем электронное письмо через Celery
    celery.send_task("app.task.tasks.send_email_task", args=[to_email, subject, body])

    return {"message": "Email отправлен"}
