# schemas.py
from pydantic import BaseModel


class EmailData(BaseModel):
    to_email: str
    subject: str
    body: str
