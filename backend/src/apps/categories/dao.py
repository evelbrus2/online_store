from .schemas import CategoryPagination, CategoryBase
from sqlalchemy import func
from sqlalchemy.exc import InvalidRequestError
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.future import select

from .models import Category


class CategoryDAO:
    @classmethod
    async def get_categories(cls, db: AsyncSession, page: int, page_size: int):
        offset = (page - 1) * page_size
        query = select(Category).order_by(Category.name).offset(offset).limit(page_size)
        total_query = select(func.count(Category.id))

        total_result = await db.execute(total_query)
        total = total_result.scalar_one()

        result = await db.execute(query)
        categories = result.scalars().all()

        return CategoryPagination(
            items=[CategoryBase.from_orm(category) for category in categories],
            total=total,
            page=page,
            page_size=page_size,
        )

    @classmethod
    async def get_category(cls, category_id: int, db: AsyncSession):
        result = await db.execute(select(Category).where(Category.id == category_id))
        category = result.scalar()
        return category

    @classmethod
    async def get_category_by_name(cls, name: str, db: AsyncSession):
        result = await db.execute(select(Category).filter(Category.name == name))
        category = result.scalar()
        return category

    @classmethod
    async def create_category(cls, category_data: dict, db: AsyncSession):
        new_category = Category(**category_data)
        db.add(new_category)
        await db.flush()  # Здесь flush используется, чтобы получить ID новой категории, если он нужен
        try:
            await db.commit()
        except InvalidRequestError:
            await db.rollback()
            raise
        return new_category

    @classmethod
    async def update_category(cls, category_id: int, category_data: dict, db: AsyncSession):
        if db.in_transaction():
            await db.commit()  # Подтвердить любые висящие транзакции перед началом новой
        await db.begin()  # Начинаем новую транзакцию
        try:
            category_query = await db.execute(select(Category).filter_by(id=category_id))
            existing_category = category_query.scalar()
            if existing_category:
                for key, value in category_data.items():
                    setattr(existing_category, key, value)
                await db.commit()  # Подтверждаем изменения
                return existing_category
            else:
                await db.rollback()  # Откатываем, если категория не найдена
                return None
        except InvalidRequestError:
            await db.rollback()
            raise

    @classmethod
    async def delete_category(cls, category_id: int, db: AsyncSession):
        if db.in_transaction():
            await db.commit()  # Подтвердить любые висящие транзакции перед началом новой
        await db.begin()  # Начинаем новую транзакцию
        try:
            category_query = await db.execute(select(Category).filter_by(id=category_id))
            existing_category = category_query.scalar()
            if existing_category:
                await db.delete(existing_category)
                await db.commit()  # Подтверждаем удаление
                return existing_category
            else:
                await db.rollback()  # Откатываем, если категория не найдена
                return None
        except InvalidRequestError:
            await db.rollback()
            raise
