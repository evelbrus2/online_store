import asyncio
import logging

from fastapi_cache.decorator import cache
from fastapi.responses import JSONResponse
from starlette import status
from fastapi import APIRouter, Depends, HTTPException, Security
from sqlalchemy.ext.asyncio import AsyncSession

from config.database import get_db
from apps.authorize.dependencies import get_current_user, is_superuser_or_staff
from apps.users.models import Users
from .schemas import CategoryBase, CategoryPagination
from .dao import CategoryDAO

# Настройте логгирование
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

router = APIRouter(
    prefix="/categories",
    tags=["Категории"]
)


@router.get("/categories", response_model=CategoryPagination)  # Обновление модели ответа
async def get_categories(
    page: int = 1,
    page_size: int = 10,  # Изменение названий параметров для пагинации
    db: AsyncSession = Depends(get_db)
):
    """
    Список категорий с пагинацией.
    """
    try:
        logger.info("Запрос списка категорий с пагинацией.")
        categories = await CategoryDAO.get_categories(db, page=page, page_size=page_size)
        return categories
    except Exception as e:
        logger.error(f"Ошибка при получении списка категорий: {e}")
        raise HTTPException(status_code=500, detail=str(e))


@router.get("/{category_id}", response_model=CategoryBase)
async def get_category(category_id: int, db: AsyncSession = Depends(get_db)):
    """
    Отображение детальной категории.
    """
    logger.info(" Запрос об определенной категории.")
    category = await CategoryDAO.get_category(category_id, db)
    if category is None:
        raise HTTPException(status_code=404, detail="Категория не найдена")
    return category


# @router.post("", response_model=CategoryBase)
# async def create_category(category_data: CategoryCreateBase,
#                           current_user: Users = Depends(get_current_user),
#                           is_authorized: bool = Security(is_superuser_or_staff),
#                           db: AsyncSession = Depends(get_db)):
#     """
#     Запрос на создание категории.
#     """
#     logger.info(" Запрос на создание категории.")
#     # Проверяем, существует ли категория с таким именем
#     existing_category = await CategoryDAO.get_category_by_name(category_data.name, db)
#     if existing_category:
#         raise HTTPException(
#             status_code=status.HTTP_400_BAD_REQUEST,
#             detail="Категория с таким именем уже существует."
#         )
#
#     # Создаем новую категорию
#     category = await CategoryDAO.create_category(category_data.dict(), db)
#
#     return CategoryBase(
#         id=category.id,
#         name=category.name
#     )
#
#
# @router.patch("/{category_id}", response_model=CategoryBase)
# async def update_category(
#         category_id: int,
#         category_data: CategoryUpdateRequest,
#         current_user: Users = Depends(get_current_user),
#         is_authorized: bool = Security(is_superuser_or_staff),
#         db: AsyncSession = Depends(get_db),
# ):
#     """
#     Запрос на обновление категории с идентификатором.
#     """
#     logger.info(f"Запрос на обновление категории с идентификатором {category_id}.")
#     try:
#         existing_category = await CategoryDAO.update_category(category_id, category_data.dict(), db)
#         if existing_category:
#             return existing_category
#         else:
#             raise HTTPException(
#                 status_code=status.HTTP_404_NOT_FOUND,
#                 detail=f"Категория с идентификатором {category_id} не существует."
#             )
#     except HTTPException as http_exc:  # Специфические исключения первыми
#         logger.error(f"HTTP exception occurred: {http_exc.detail}")
#         raise http_exc
#     except Exception as e:
#         logger.error(f"Неожиданная ошибка: {type(e).__name__}, {e.__dict__}")
#         raise HTTPException(
#             status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
#             detail="Произошла непредвиденная ошибка."
#         )
#
#
# @router.delete("/{category_id}", response_class=JSONResponse, status_code=status.HTTP_204_NO_CONTENT)
# async def delete_category(category_id: int,
#                           current_user: Users = Depends(get_current_user),
#                           is_authorized: bool = Security(is_superuser_or_staff),
#                           db: AsyncSession = Depends(get_db)):
#     """
#     Запрос на удаление подкатегории.
#     """
#     logger.info(" Запрос на удаление подкатегории.")
#     existing_category = await CategoryDAO.delete_category(category_id, db)
#     if existing_category:
#         raise HTTPException(
#             status_code=status.HTTP_200_OK,
#             detail=f"Успешно удалена категория с ID {category_id}"
#         )
#     else:
#         raise HTTPException(status_code=404, detail="Категория не найдена")
