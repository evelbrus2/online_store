from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship


from config.database import Base


class Category(Base):
    __tablename__ = "categories"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, index=True, unique=True, nullable=False)

    subcategories = relationship("Subcategory", back_populates="category")

    def __str__(self):
        return f"{self.name}"

    class Config:
        orm_mode = True
