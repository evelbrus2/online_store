from typing import Optional, List

from pydantic import BaseModel


class CategoryBase(BaseModel):
    id: int
    name: str

    class Config:
        from_attributes = True


class CategoryCreateBase(BaseModel):
    name: str

    class Config:
        from_attributes = True


class CategoryUpdateRequest(BaseModel):
    name: Optional[str] = None

    class Config:
        from_attributes = True

class CategoryPagination(BaseModel):
    items: List[CategoryBase]
    total: int
    page: int
    page_size: int

    class Config:
        from_attributes = True