from fastapi_cache.backends import redis
from redis import asyncio as aioredis
from fastapi_cache import FastAPICache
from fastapi_cache.backends.redis import RedisBackend

# Глобальная переменная для хранения клиента Redis
redis_client = None

async def on_startup():
    global redis_client
    # Инициализация Redis при запуске приложения
    redis_client = aioredis.from_url("redis://localhost:6379")
    FastAPICache.init(RedisBackend(redis_client), prefix="cache")


async def on_shutdown():
    global redis_client
    # Закрытие соединения с Redis при завершении работы приложения
    if redis_client:
        await redis_client.close()
