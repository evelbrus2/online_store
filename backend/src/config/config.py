import os
from typing import Literal

from pydantic_settings import BaseSettings
from pydantic import field_validator


class Settings(BaseSettings):
    some_setting: str = "значение_по_умолчанию"
    another_setting: int = 123

    MODE: Literal["DEV", "TEST", "PROD"]

    DATABASE_URL: str = None
    SECRET_KEY: str
    ALGORITHM: str
    ACCESS_TOKEN_EXPIRE_MINUTES: int
    REFRESH_TOKEN_EXPIRE_DAYS: int
    ENV: str

    @field_validator('DATABASE_URL')
    def assemble_database_url(cls, v: str, values: dict) -> str:
        if v is None:
            return (
                f"postgresql+asyncpg://{values['DB_USER']}:"
                f"{values['DB_PASSWORD']}@"
                f"{values['DB_HOST']}:"
                f"{values['DB_PORT']}/"
                f"{values['DB_NAME']}"
            )
        return v

    # DATABASE_URL_TEST: str = None
    #
    # @field_validator('DATABASE_URL_TEST')
    # def assemble_database_url_test(cls, v: str, values: dict) -> str:
    #     if v is None:
    #         return (
    #             f"postgresql+asyncpg://{values['DB_USER_TEST']}:"
    #             f"{values['DB_PASSWORD_TEST']}@"
    #             f"{values['DB_HOST_TEST']}:"
    #             f"{values['DB_PORT_TEST']}/"
    #             f"{values['DB_NAME_TEST']}"
    #         )
    #     return v

    @field_validator('SECRET_KEY')
    def assemble_secret_key(cls, v: str, values: dict) -> str:
        if v is None:
            return os.environ.get.SECRET_KEY
        return v

    @field_validator('ALGORITHM')
    def assemble_algorithm(cls, v: str, values: dict) -> str:
        if v is None:
            return os.environ.get.ALGORITHM
        return v

    @field_validator('ACCESS_TOKEN_EXPIRE_MINUTES')
    def assemble_access_token_expire_minutes(cls, v: int, values: dict) -> int:
        if v is None:
            return os.environ.get.ACCESS_TOKEN_EXPIRE_MINUTES
        return v

    @field_validator('REFRESH_TOKEN_EXPIRE_DAYS')
    def assemble_refresh_token_expire_days(cls, v: int, values: dict) -> int:
        if v is None:
            return os.environ.get.REFRESH_TOKEN_EXPIRE_DAYS
        return v

    @field_validator('ENV')
    def assemble_env(cls, v: str, values: dict) -> str:
        if v is None:
            return os.environ.get.ENV
        return v

    class Config:
        env_file = '.env'
        env_file_encoding = 'utf-8'
        env_file_encoding_errors = 'ignore'
        env_file_mode = '0644'


settings = Settings(_env_file='.env')
print(settings)