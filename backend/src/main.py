import logging
import shutil
import time

import uvicorn

from fastapi import FastAPI, Request, Response
from fastapi.responses import JSONResponse
from starlette.middleware.cors import CORSMiddleware
from config.config import settings

from config.lifespan import on_startup, on_shutdown
from apps.authorize.exceptions import DatabaseConnectionException, ExternalServiceException
from apps.authorize.router import router as router_authorize
from apps.users.router import router as router_users
from apps.base.router import router as router_base
from apps.categories.router import router as router_categories
from apps.subcategery.router import router as router_subcategery
from apps.products.router import router as router_products
from apps.colors.router import router as router_colors
from apps.basket.router import router as router_basket
from apps.email_visa.router import router as router_email_vise
from apps.prometheus.router import router as router_prometheus

from apps.admin.adminpanel import setup_admin
from config.database import async_session_maker

app = FastAPI()

#################################################################
##                                                             ##
##                       SENTRY START                          ##
##                                                             ##
#################################################################

import sentry_sdk

sentry_sdk.init(
    dsn="https://d81f755f6dff2891bde06e01c4bb17c1@o4506260174864384.ingest.sentry.io/4506260176699392",
    traces_sample_rate=1.0,
    profiles_sample_rate=1.0,
)

#################################################################
##                                                             ##
##                       SENTRY END                            ##
##                                                             ##
#################################################################


#################################################################
##                                                             ##
##                       PROMETHEUS START                      ##
##                                                             ##
#################################################################

from prometheus_fastapi_instrumentator import Instrumentator

instrumentator = Instrumentator(
    should_group_status_codes=False,
    should_ignore_untemplated=True,
    should_respect_env_var=True,
    should_instrument_requests_inprogress=True,
    excluded_handlers=[".*admin.*", "/metrics"],
    env_var_name="ENABLE_METRICS",
    inprogress_name="inprogress",
    inprogress_labels=True,
)

Instrumentator().instrument(app).expose(app)


#################################################################
##                                                             ##
##                       PROMETHEUS END                        ##
##                                                             ##
#################################################################

app.include_router(router_authorize)
app.include_router(router_users)
app.include_router(router_base)
app.include_router(router_categories)
app.include_router(router_subcategery)
app.include_router(router_products)
app.include_router(router_colors)
app.include_router(router_basket)
app.include_router(router_email_vise)
app.include_router(router_prometheus)

origins = [
    "http://localhost:3000",
    "http://localhost:8000",
    "http://localhost:5000",
    "https://aress2245.store",
    "http://aress2245.store",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["GET", "POST", "OPTIONS", "DELETE", "PATCH", "PUT"],
    allow_headers=["Content-Type", 'Set-Cookie', "Access-Control-Allow-Headers", "Access-Control-Allow-Origin",
                   "Authorization"],
    max_age=3600,
)

@app.middleware("http")
async def db_session_middleware(request: Request, call_next):
    response = Response("Internal server error", status_code=500)
    try:
        request.state.db = async_session_maker()
        response = await call_next(request)
    finally:
        await request.state.db.close()
    return response

app.middleware("http")(db_session_middleware)

#################################################################
##                                                             ##
##                       LOGGING  START                        ##
##                                                             ##
#################################################################


if settings.ENV == 'production':
    log_level = logging.INFO
else:
    log_level = logging.DEBUG

logging.basicConfig(level=log_level)
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
# Добавьте обработчик, который выводит логи в консоль с указанным форматом
ch = logging.StreamHandler()
ch.setFormatter(formatter)
logger.addHandler(ch)

logger.debug(f"Текущее окружение: {settings.ENV}")
logger.info(f"Установлен уровень логирования: {logging.getLevelName(logger.getEffectiveLevel())}")

@app.middleware("http")
async def add_process_time_header(request: Request, call_next):
    start_time = time.time()
    response = await call_next(request)
    process_time = time.time() - start_time
    logger.info("###################ВРЕМЯ ВЫПОЛНЕНИЯ ЗАПРОСА: %.4f СЕКУНДЫ#################", process_time)
    return response


#################################################################
##                                                             ##
##                       LOGGING  END                          ##
##                                                             ##
#################################################################

@app.exception_handler(DatabaseConnectionException)
async def database_exception_handler(request: Request, exc: DatabaseConnectionException):
    return JSONResponse(
        status_code=exc.status_code,
        content={"detail": exc.detail}
    )


@app.exception_handler(ExternalServiceException)
async def external_service_exception_handler(request: Request, exc: ExternalServiceException):
    return JSONResponse(
        status_code=exc.status_code,
        content={"detail": exc.detail}
    )

from fastapi import FastAPI, UploadFile, File, HTTPException
from typing import List

@app.post("/uploadfiles/")
async def upload_files(files: List[UploadFile] = File(...)):
    for file in files:
        try:
            # Сохранение файла, например, на диск или в базу данных
            with open(f"some_directory/{file.filename}", "wb") as buffer:
                shutil.copyfileobj(file.file, buffer)
        finally:
            await file.close()
    return {"message": "Файлы успешно загружены"}

redis_pool = None

#################################################################
##                                                             ##
##                       REDIS  START                          ##
##                                                             ##
#################################################################

app.add_event_handler("startup", on_startup)
app.add_event_handler("shutdown", on_shutdown)

#################################################################
##                                                             ##
##                       REDIS END                             ##
##                                                             ##
#################################################################

admin = setup_admin(app)

if __name__ == "__main__":
    uvicorn.run("main:app", host="0.0.0.0", port=8000, reload=True)
