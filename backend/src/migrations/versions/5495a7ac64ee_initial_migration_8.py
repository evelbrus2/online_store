"""Initial migration 8

Revision ID: 5495a7ac64ee
Revises: f5b3378e66ff
Create Date: 2023-11-22 10:08:32.605659

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

from fastapi_storages import FileSystemStorage
from fastapi_storages.integrations.sqlalchemy import FileType

storage = FileSystemStorage(path="/home/valentin/Desktop/FastApi/FAOS3/Canada/canada/backend/src/apps/images/uploads")

# revision identifiers, used by Alembic.
revision: str = '5495a7ac64ee'
down_revision: Union[str, None] = 'f5b3378e66ff'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('products', sa.Column('images_1', sa.ARRAY(FileType(storage=storage)), nullable=True))
    op.add_column('products', sa.Column('images_2', sa.ARRAY(FileType(storage=storage)), nullable=True))
    op.add_column('products', sa.Column('images_3', sa.ARRAY(FileType(storage=storage)), nullable=True))
    op.add_column('products', sa.Column('images_4', sa.ARRAY(FileType(storage=storage)), nullable=True))
    op.add_column('products', sa.Column('images_5', sa.ARRAY(FileType(storage=storage)), nullable=True))
    op.add_column('products', sa.Column('images_6', sa.ARRAY(FileType(storage=storage)), nullable=True))
    op.add_column('products', sa.Column('images_7', sa.ARRAY(FileType(storage=storage)), nullable=True))
    op.add_column('products', sa.Column('images_8', sa.ARRAY(FileType(storage=storage)), nullable=True))
    op.add_column('products', sa.Column('images_9', sa.ARRAY(FileType(storage=storage)), nullable=True))
    op.drop_column('products', 'images')
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('products', sa.Column('images', postgresql.BYTEA(), autoincrement=False, nullable=True))
    op.drop_column('products', 'images_9')
    op.drop_column('products', 'images_8')
    op.drop_column('products', 'images_7')
    op.drop_column('products', 'images_6')
    op.drop_column('products', 'images_5')
    op.drop_column('products', 'images_4')
    op.drop_column('products', 'images_3')
    op.drop_column('products', 'images_2')
    op.drop_column('products', 'images_1')
    # ### end Alembic commands ###
